module load cuda/8.0
module load gcc/5.4.0

nvcc -c -arch=sm_30 device.cu
gcc -I$CUDA_HOME/inlcude -c input.c
gcc -I$CUDA_HOME/inlcude -c calc_cuda.c
gcc -I$CUDA_HOME/inlcude -c main_func.c
gcc -I$CUDA_HOME/inlcude -c xdrfile.c
gcc -I$CUDA_HOME/inlcude -c xdrfile_xtc.c

gcc -I$CUDA_HOME/inlcude  -L${CUDA_HOME}/lib64 -o filter.x input.o xdrfile.o xdrfile_xtc.o  main_func.o calc_cuda.o device.o -lcuda -lcudart -lm -lstdc++
