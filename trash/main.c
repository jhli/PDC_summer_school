/*
 * jhli@TCB 27-08-2018
 *
 * The code is refered to antechamber and mobywat
 *
 * Please implemented it to your code if you want to read the infomation rather than print it.
 *
 * To compile the code, just type: make all
 *
 * Remember to "make clean" before re-compiling the code.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "typstruct.h" 

int rpdb(char * filename, int * atomnum, int * watnum, ATOM * atom);
int rxtc(char * filename, int atomnum, int watnum, ATOM * atom, awt * mywat, vt * boxct, int * frames, int * len_awt);
int checkout(int len_awt, awt * mywat, vt * boxct);
int findwat(int len_awt, awt * mywat, int watnum, int frames);

int main(int argc, char *argv[])
{
   if (argc < 3) {
      printf("Error, please input the topology(pdb) and trajectory(xtc)\n");
      exit(1);
   }
   int atomnum, watnum, frames, len_awt;
   vt  boxct;
   ATOM * atom;
   awt  * mywat;
   atom  = (ATOM *) malloc(sizeof(ATOM) * MAXATOM);
   mywat = (awt *) malloc(sizeof(awt) * MAXWAT);

   rpdb(argv[1], &atomnum, &watnum, atom);
   //printf("\natoms %d; water %d\n", atomnum, watnum);
   //printf("%s\n", "frame, step, time, wat_nam, wat_id, atom_id, x, y, z");
   rxtc(argv[2], atomnum, watnum, atom, mywat, &boxct, &frames, &len_awt);
   
   //uncomment the follow two lines if one wants to check the reading of water in xtc!
   //printf("awt_len is %d, watnum * frames is %d\n", len_awt, watnum * (frames - 1));
   //checkout(len_awt, mywat, &boxct);
   
   findwat(len_awt, mywat, watnum, frames);
   
   free(atom);
   free(mywat);
   atom = NULL;
   mywat = NULL;
   return 0;
}
