#!/bin/bash
NPT="./npt.pdb"
XTC="../prod.xtc"
PROCS="10"
EXEC="hydra_site.x"
GPU="gtx1070"
TIMMING="timming_O2_beta_run1.info"

mkdir -p outputs

echo "# OMP_NUM_THREADS, input_reading(s), findwat(s), iteration(s), TOTAL duration (s)" > $TIMMING

for i in $(seq 1 $PROCS); do
   export OMP_NUM_THREADS=$i
   $EXEC $NPT $XTC >& out_omp${i}
   mv WatSites.pdb outputs/WatSites_omp${i}.pdb
   rein=$(grep "input_reading" out_omp${i} | awk '{print $5}')
   fiwa=$(grep "findwat()"     out_omp${i} | awk '{print $5}')
   iter=$(grep "iteration()"   out_omp${i} | awk '{print $5}')
   tota=$(grep "TOTAL"         out_omp${i} | awk '{print $5}')
   mv out_omp${i} outputs
   echo "$i $rein   $fiwa   $iter   $tota" >> $TIMMING
done

# should be running alone
#echo "# CUDA VERSION $GPU"
#
#cd cuda_version
#
#$EXEC ../$NPT ../$XTC >& out_cuda_${GPU}
#
#rein=$(grep "reading input" out_cuda_${GPU} | awk '{print $5}')
#fiwa=$(grep "findwat()"     out_cuda_${GPU} | awk '{print $5}')
#iter=$(grep "iteration()"   out_cuda_${GPU} | awk '{print $5}')
#tota=$(grep "TOTAL"         out_cuda_${GPU} | awk '{print $5}')
#
#echo "$i $rein   $fiwa   $iter   $tota" >> ../$TIMMING
