# Test parameters

center = "23.800  29.199  25.353" #angstrom

boxsize = 0.8 #nm

grid_size = 0.12 #nm

DENSITY=0.2

cutoff = 0.13 #nm

iteration_times=20

# Reference file

crystal_water.pdb

# Python result
calculated_water.pdb

calculated by: hydro_site.py 

time useage: 46069.835955 s

