#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<memory.h>
#include<limits.h>
#include "typstruct.h"
#include "xdrfile.h"
#include "xdrfile_xtc.h"


int rpdb(char * filename, int * atomnum, int * watnum, ATOM * atom)
{
   int id            = -1;
   int terindex      = 0;
   int numatom       = 0;
   int numwat        = 0;
   int resno         = -1;
   char atomname[10] = {"INITIAL"};
   char resname[20]  = {"INITIAL"};
   char line[LINELEN_MAX];
   double x, y, z;
   int tmpint;
   FILE *fpin;

   if ((fpin = fopen(filename, "r")) == NULL) {
      fprintf(stdout, "Error: Cannot open the input file: %s in rpdb(), exit\n", filename);
      exit(1);
   }
   //initial(50, atom);
   for (;;) {
      if (fgets(line, LINELEN_MAX, fpin) == NULL) {
         break;
      }

      if (strncmp("TER", line, 3) == 0) {
         terindex = 1;
         continue;
      }

      if (strncmp("ATOM", line, 4) == 0 || strncmp("HETATM", line, 6) == 0) {
         atomname[0] = line[12];
         atomname[1] = line[13];
         atomname[2] = line[14];
         atomname[3] = line[15];
         if (line[16] == ' ') {
            atomname[4] = '\0';          
         }
         else {
            atomname[4] = line[16];
            atomname[5] = '\0';
         }
         
         if (strchr("0123456789", line[5]) != NULL) {
            sscanf(line + 5, "%d", &id); /* 'ATOM\s\d+' */
         }
         else {
            sscanf(line + 6, "%d", &id); /* 'HETATM' or 'ATOM\s\s' */
         }
         resname[0] = line[17];
         resname[1] = line[18];
         resname[2] = line[19];

         if (line[20] != ' ') {
            resname[3] = line[20];
            resname[4] = '\0';
         }
         else {
            resname[3] = '\0'; 
         }

         sscanf(&line[22], "%d%lf%lf%lf", &tmpint, &x, &y, &z);
         resno = tmpint;

         if (strcmp(resname, "SOL") == 0 && strcmp(atomname, " OW ") == 0) {
            numwat ++;
         }

         atom[numatom].id = id;
         strcpy(atom[numatom].name, atomname);
         strcpy(atom[numatom].aa, resname);
         atom[numatom].resno = resno;
         atom[numatom].x = x;
         atom[numatom].y = y;
         atom[numatom].z = z;

         numatom++;
      }
   }
   //sscanf(&numatom, "%d", atomnum);
   *atomnum = numatom;
   *watnum  = numwat;
   fclose(fpin);
   //return numatom;
   return 0;
}  


int rxtc(char * filename, int atomnum, int watnum, ATOM * atom, awt * mywat, vt * boxct, int * frames, int * len_awt)
{
   int natoms, natom, step, read_return, numwat;
   float time, p;
   FILE *fpin;
   int    frame = 0;
   int tot_wat  = 1;
   double diag1 = 0.0;
   double diag2 = 0.0;
   double diag3 = 0.0;

   if ((fpin = fopen(filename, "r")) == NULL) {
      fprintf(stdout, "Cannot open the input file: %s in rxtc(), exit\n",
                     filename);
      exit(1);
   }
  
   matrix box;
   rvec *x;
   XDRFILE *xtc;
   xtc = xdrfile_open(filename, "r");
   read_return = read_xtc_natoms(filename, &natoms);
   x = (rvec *)calloc(natoms, sizeof(x[0]));

   if (natoms != atomnum) {
      printf("ERROR: Mismatch between trajectory (%d atoms) and topology (%d atoms)\n", natoms,atomnum);
      exit(1);
   }

   while (1)
   {
      read_return = read_xtc(xtc,natoms,&step,&time,box,x,&p);
      if (read_return != 0)
      {
         boxct->v1 = 0.5 * diag1 / frame; // This is the basis vector, it needs to multiply the real box size!
         boxct->v2 = 0.5 * diag2 / frame;
         boxct->v3 = 0.5 * diag3 / frame;
         //printf("\nbox center: %lf %lf %lf\n", boxct->v1, boxct->v2, boxct->v3);
         //printf("\n The smallest edges of the box: %lf %lf %lf\n", 2*boxct->v1, 2*boxct->v2, 2*boxct->v3);
         break;
      }
      
      if (frame == 0) { //abort the first frame, which is the topology with the x of (0, 0, 0)
         frame = 1;
         continue;
      }

      diag1 += box[0][0];
      diag2 += box[1][1];
      diag3 += box[2][2];
      
      numwat = 1;
      for (natom=0;natom<=natoms;natom++)
      {
         if (strcmp(atom[natom].aa, "SOL") == 0 && strcmp(atom[natom].name, " OW ") == 0) { /* modify if one want some specifications */
            mywat[tot_wat].fid = frame; 
            mywat[tot_wat].nid = numwat;
            mywat[tot_wat].aid = atom[natom].id;
            mywat[tot_wat].x   = x[natom][0];
            mywat[tot_wat].y   = x[natom][1];
            mywat[tot_wat].z   = x[natom][2];
            //printf("%d %d %f %d %f %f %f\n",atom[natom].id, frame,time,natom,x[natom][0],x[natom][1],x[natom][2]); // print 31155000 lines 
            numwat ++;
            tot_wat ++;
         }
      }
      
      frame ++;
   }

   *frames  = frame;
   *len_awt = tot_wat;
   xdrfile_close(xtc);
   return 0;
}

